package org.porgrad.tw;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class LastSundayTest {

    LastSunday lastSunday = new LastSunday();

    @Test
    void shouldReturnTheLastSundayOfEveryMonthInTheGivenYear() {

        ArrayList<Integer> actualResult = lastSunday.getLastSunday(2022);
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(30, 27, 27, 24, 29, 26, 31, 28, 25, 30, 27, 25));

        assertThat(actualResult, is(equalTo(expectedResult)));
    }
}
